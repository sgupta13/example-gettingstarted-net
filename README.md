# e-SignLive GettingStarted Sample #

This working e-SignLive sample code refers to the C# sample at http://docs.e-signlive.com/doku.php?id=esl:e-signlive_getting_started#creating_your_first_document_package

This project contains a test class for you to run in package

    GettingStartedTest.cs
    
Make sure you set your own eSL API key obtained from your e-SignLive sandbox account in the Resources file prior to executing it.
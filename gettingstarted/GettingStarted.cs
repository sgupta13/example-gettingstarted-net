﻿using Silanis.ESL.SDK;
using Silanis.ESL.SDK.Builder;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace gettingstarted
{
    public class GettingStarted
    {
        public static readonly String API_URL = "https://sandbox.e-signlive.com/api"; // USE https://apps.e-signlive.com/api FOR PRODUCTION

        public static void Main(string[] args)
        {
            String API_KEY = Properties.Resources.API_KEY;
            EslClient eslClient = new EslClient(API_KEY, API_URL);

            Stream file = new MemoryStream(Properties.Resources.test);

            DocumentPackage superDuperPackage = PackageBuilder.NewPackageNamed("GettingStartedExample: " + DateTime.Now)
                .WithSettings(DocumentPackageSettingsBuilder.NewDocumentPackageSettings())
                    .WithSigner(SignerBuilder.NewSignerWithEmail("johnsmith@mailinator.com")
                                .WithFirstName("John")
                                .WithLastName("Smith")
                                .WithCustomId("SIGNER1"))
                    .WithDocument(DocumentBuilder.NewDocumentNamed("Test document")
                                  .FromStream(file, DocumentType.PDF)
                                  .WithSignature(SignatureBuilder.SignatureFor("johnsmith@mailinator.com")
                                   .OnPage(0)
                                   .AtPosition(100, 100))
                                  .WithSignature(SignatureBuilder.InitialsFor("johnsmith@mailinator.com")
                                   .OnPage(0)
                                   .AtPosition(100, 200))
                                  .WithSignature(SignatureBuilder.CaptureFor("johnsmith@mailinator.com")
                                   .OnPage(0)
                                   .AtPosition(100, 300))
                                  )
                    .Build();
 
            PackageId packageId = eslClient.CreatePackage(superDuperPackage);
            eslClient.SendPackage(packageId);
 
            // Optionally, get the session token for integrated signing.
            SessionToken sessionToken = eslClient.SessionService.CreateSessionToken(packageId, "SIGNER1");
        }
    }
}